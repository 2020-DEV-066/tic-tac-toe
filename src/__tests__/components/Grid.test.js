import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Grid from '../../components/Grid';

import {
    BOX_TESTID,
    RESET_GAME,
    INITIAL_GAME_STATE,
} from '../../utils/constants';

describe('Grid', () => {
    it('should render properly', () => {
        const { baseElement } = render(<Grid state={INITIAL_GAME_STATE} />);
        expect(baseElement).toBeTruthy();
    });
    it('should render 9 boxes by default', () => {
        const { getAllByTestId } = render(<Grid state={INITIAL_GAME_STATE} />);
        expect(getAllByTestId(BOX_TESTID).length).toBe(9);
    });
    it('should have a reset button', () => {
        const { getByText } = render(<Grid state={INITIAL_GAME_STATE} />);
        expect(getByText(RESET_GAME)).toBeTruthy();
    });
    it('should run the function passed as prop when a box is clicked', () => {
        const mockFunction = jest.fn();
        const { getAllByTestId } = render(
            <Grid state={INITIAL_GAME_STATE} handleBoxClick={mockFunction} />
        );
        fireEvent.click(getAllByTestId(BOX_TESTID)[0]);
        expect(mockFunction).toHaveBeenCalled();
    });
});
