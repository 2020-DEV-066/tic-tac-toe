import React from 'react';
import { render } from '@testing-library/react';
import GameStatus from '../../components/GameStatus';

import { WINNER_X } from '../../utils/constants';

describe('GameStatus', () => {
    it('should render properly', () => {
        const { baseElement } = render(<GameStatus />);
        expect(baseElement).toBeTruthy();
    });
    it('should display the prop status value', () => {
        const { getByText } = render(<GameStatus status={WINNER_X} />);
        expect(getByText(WINNER_X)).toBeDefined();
    });
});
