import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Box from '../../components/Box';

import { BOX_TESTID, PLAYER_X } from '../../utils/constants';

const voidFunction = jest.fn();

describe('Box', () => {
    it('should render properly', () => {
        const { baseElement } = render(<Box onClick={voidFunction} />);
        expect(baseElement).toBeTruthy();
    });
    it('should be empty if no value is passed', () => {
        const { getByTestId } = render(<Box onClick={voidFunction} />);
        expect(getByTestId(BOX_TESTID).innerHTML.length).toBe(0);
    });
    it('should have a value matching the string passed as a prop', () => {
        const { getByText } = render(
            <Box value={PLAYER_X} onClick={voidFunction} />
        );
        expect(getByText(PLAYER_X)).toBeDefined();
    });
    it('should run the passed function on click', () => {
        const mockFunction = jest.fn();
        const { getByTestId } = render(<Box onClick={mockFunction} />);
        fireEvent.click(getByTestId(BOX_TESTID));
        expect(mockFunction).toHaveBeenCalled();
    });
    it('should not call function when it already has a value', () => {
        const mockFunction = jest.fn();
        const { getByTestId } = render(
            <Box value={PLAYER_X} onClick={mockFunction} />
        );
        fireEvent.click(getByTestId(BOX_TESTID));
        expect(mockFunction).not.toHaveBeenCalled();
    });
});
