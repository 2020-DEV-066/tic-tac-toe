import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import ResetButton from '../../components/ResetButton';

import { RESET_GAME } from '../../utils/constants';

describe('ResetButton', () => {
    it('should render properly', () => {
        const { baseElement } = render(<ResetButton />);
        expect(baseElement).toBeTruthy();
    });
    it('should contains the right text', () => {
        const { getByText } = render(<ResetButton />);
        expect(getByText(RESET_GAME)).toBeDefined();
    });
    it('should fire the prop function on click', () => {
        const mockFunction = jest.fn();
        const { getByText } = render(
            <ResetButton handleReset={mockFunction} />
        );
        fireEvent.click(getByText(RESET_GAME));
        expect(mockFunction).toHaveBeenCalled();
    });
});
