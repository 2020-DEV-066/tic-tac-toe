import React from 'react';
import { render } from '@testing-library/react';
import App from '../../components/App';
import { APP_TITLE } from '../../utils/constants';

describe('App', () => {
    it('should render properly', () => {
        const { baseElement } = render(<App />);
        expect(baseElement).toBeTruthy();
    });
    it('should display the app title', () => {
        const { getByText } = render(<App />);
        expect(getByText(APP_TITLE)).toBeDefined();
    });
});
