import React from 'react';
import { render } from '@testing-library/react';
import each from 'jest-each';
import GameStatusContainer from '../../containers/GameStatusContainer';

import { WINNER_X, WINNER_O, DRAW_GAME } from '../../utils/constants';

const scenarios = [
    {
        name: 'horizontalWin',
        status: WINNER_X,
        gameState: {
            boxes: ['X', 'X', 'X', 'O', null, null, 'O', null, null],
            xIsNext: false,
        },
    },
    {
        name: 'verticalWin',
        status: WINNER_O,
        gameState: {
            boxes: [null, 'O', 'X', null, 'O', 'X', 'X', 'O', null],
            xIsNext: true,
        },
    },
    {
        name: 'diagonalWin1',
        status: WINNER_X,
        gameState: {
            boxes: ['X', 'O', null, null, 'X', 'O', null, null, 'X'],
            xIsNext: false,
        },
    },
    {
        name: 'diagonalWin2',
        status: WINNER_O,
        gameState: {
            boxes: ['X', null, 'O', 'X', 'O', null, 'O', null, 'X'],
            xIsNext: true,
        },
    },
    {
        name: 'draw1',
        status: DRAW_GAME,
        gameState: {
            boxes: ['X', 'O', 'X', 'X', 'X', 'O', 'O', 'X', 'O'],
            xIsNext: false,
        },
    },
    {
        name: 'draw2',
        status: DRAW_GAME,
        gameState: {
            boxes: ['O', 'O', 'X', 'X', 'X', 'O', 'O', 'X', 'X'],
            xIsNext: false,
        },
    },
];

describe('GameStatus', () => {
    it('should render properly', () => {
        const { baseElement } = render(<GameStatusContainer />);
        expect(baseElement).toBeTruthy();
    });
    each([scenarios]).test(
        'if the board result (win or draw) is the expected one',
        (scenario) => {
            const { getByText } = render(
                <GameStatusContainer state={scenario.gameState} />
            );
            expect(getByText(scenario.status)).toBeTruthy();
        }
    );
});
