import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import GridContainer from '../../containers/GridContainer';

import { BOX_TESTID, RESET_GAME } from '../../utils/constants';

describe('GridContainer', () => {
    it('should render properly', () => {
        const { baseElement } = render(<GridContainer />);
        expect(baseElement).toBeTruthy();
    });
    it('should be set to initial value when user clicks on the reset button during the game', () => {
        const { getAllByTestId, getByText } = render(<GridContainer />);
        const boxes = getAllByTestId(BOX_TESTID);

        for (let i = 0; i < 3; i++) {
            fireEvent.click(boxes[i]);
        }
        fireEvent.click(getByText(RESET_GAME));
        expect(boxes.every((el) => !el.innerHTML)).toBeTruthy();
    });
});
