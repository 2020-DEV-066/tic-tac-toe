/*
  GridContainer contains the game's state and all the methods to
  set a new state, as well as resetting it.
*/
import React, { useState } from 'react';

import Grid from '../components/Grid';

import { INITIAL_GAME_STATE, PLAYER_X, PLAYER_O } from '../utils/constants';

export const GridContainer = ({ providedState = null }) => {
    const [state, setState] = useState(providedState || INITIAL_GAME_STATE);

    const handleBoxClick = (boxIndex) => {
        const currentPlayer = state.xIsNext ? PLAYER_X : PLAYER_O;
        setState({
            boxes: state.boxes.map((el, index) =>
                index === boxIndex ? currentPlayer : el
            ),
            xIsNext: !state.xIsNext,
        });
    };

    const resetGame = (e) => {
        e.preventDefault();
        setState(INITIAL_GAME_STATE);
    };
    return (
        <Grid
            state={state}
            handleBoxClick={handleBoxClick}
            handleReset={resetGame}
        />
    );
};

export default GridContainer;
