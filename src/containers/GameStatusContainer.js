/*
  GameStatusContainer contains all the game's logic in order
  to display the current status (victory, draw, or ongoing game)
*/
import React from 'react';

import GameStatus from '../components/GameStatus';

import {
    INITIAL_GAME_STATE,
    PLAYER_X,
    PLAYER_O,
    WINNER_X,
    WINNER_O,
    DRAW_GAME,
} from '../utils/constants';

const GameStatusContainer = ({ state = INITIAL_GAME_STATE }) => {
    /**
     *
     * @description Helper function comparing current game state with winning combinaisons
     * @param {Array} boxes The array of all the current game's boxes
     * @returns {string|null} The winner if there is one, null otherwise
     */
    const findWinner = (boxes) => {
        const winningCombinations = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];

        for (let i = 0; i < winningCombinations.length; i++) {
            const [a, b, c] = winningCombinations[i];

            if (boxes[a] && boxes[a] === boxes[b] && boxes[a] === boxes[c]) {
                return boxes[a] === PLAYER_X ? PLAYER_X : PLAYER_O;
            }
        }
        return null;
    };

    /**
     *
     * @description Loops on all boxes to determine if all the boxes are filled
     * @param {Array} boxes The array of all the current game's boxes
     * @returns {boolean}
     */
    const allBoxesFilled = (boxes) => {
        let count = 0;

        boxes.forEach((el) => (el ? count++ : el));

        return count === 9;
    };
    /**
     *
     * @description Gets the current game's status
     * @returns {string} Status message to be rendered
     */
    const getStatus = () => {
        if (findWinner(state.boxes)) {
            return findWinner(state.boxes) === PLAYER_X ? WINNER_X : WINNER_O;
        } else {
            if (allBoxesFilled(state.boxes)) {
                return DRAW_GAME;
            } else {
                return `It's ${state.xIsNext ? PLAYER_X : PLAYER_O}'s turn`;
            }
        }
    };
    const status = getStatus();
    return <GameStatus status={status} />;
};

export default GameStatusContainer;
