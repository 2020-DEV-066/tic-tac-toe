/*
  The GameStatus component takes one parameter and displays it.
  This parameter is the game status, managed by this component's
  container.
*/
import React from 'react';

const GameStatus = ({ status = null }) => {
    return <pre>{status}</pre>;
};

export default GameStatus;
