/*
  The Grid component displays everything relevant to the players:
    - the 9 boxes
    - the game status
    - the reset button
*/

import React from 'react';
import styles from './Grid.module.scss';

import Box from './Box';
import ResetButton from './ResetButton';
import GameStatusContainer from '../containers/GameStatusContainer';

const Grid = ({ state, handleBoxClick, handleReset }) => {
    return (
        <>
            <div className={styles.container}>
                {state.boxes.map((el, index) => (
                    <Box
                        key={index}
                        value={el}
                        onClick={() => {
                            handleBoxClick(index);
                        }}
                    />
                ))}
            </div>
            <GameStatusContainer state={state} />
            <ResetButton handleReset={handleReset} />
        </>
    );
};

export default Grid;
