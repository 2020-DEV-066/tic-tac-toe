/*
  The Box component displays a clickable button.
*/
import React from 'react';
import styles from './Box.module.scss';

import { BOX_TESTID } from '../utils/constants';

const Box = ({ value = null, onClick }) => {
    return (
        <button
            data-testid={BOX_TESTID}
            className={styles.box}
            onClick={() => {
                if (!value) onClick();
            }}
        >
            {value}
        </button>
    );
};

export default Box;
