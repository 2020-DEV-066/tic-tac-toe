/*
  The App component displays the app title as well as the
  game itself (GridContainer).
*/
import React from 'react';
import styles from './App.module.scss';

import GridContainer from '../containers/GridContainer';

import { APP_TITLE } from '../utils/constants';

function App() {
    return (
        <div className={styles.App}>
            <header>
                <h1 className={styles.title}>{APP_TITLE}</h1>
            </header>
            <main>
                <GridContainer />
            </main>
        </div>
    );
}

export default App;
