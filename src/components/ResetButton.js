/*
  ResetButton displays a simple "Reset" button and
  allow user to start a new game.
*/
import React from 'react';
import styles from './ResetButton.module.scss';

import { RESET_GAME } from '../utils/constants';

const ResetButton = ({ handleReset }) => {
    return (
        <button className={styles.btn} onClick={handleReset}>
            {RESET_GAME}
        </button>
    );
};

export default ResetButton;
