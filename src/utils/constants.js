// APP'S GENERAL TEXTS
export const APP_TITLE = 'Tic tac toe Kata 🥋';
export const RESET_GAME = 'Reset game';
// PLAYER'S NAMES
export const PLAYER_X = 'X';
export const PLAYER_O = 'O';
// STATUS MESSAGES
export const WINNER_X = `Player ${PLAYER_X} won!`;
export const WINNER_O = `Player ${PLAYER_O} won!`;
export const DRAW_GAME = 'Its a draw!';
// TEST IDS
export const BOX_TESTID = 'box';
// INITIAL VALUES
export const INITIAL_GAME_STATE = {
    boxes: Array(9).fill(null),
    xIsNext: true,
};
